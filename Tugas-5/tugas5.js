// SOAL 1

console.log("SOAL 1");
console.log("===============================================================");

function halo () {
	var x = '"Halo Sanbers!"';
	return x
}


console.log(halo());


console.log("===============================================================");
console.log("\n");

// SOAL 2

console.log("SOAL 2");
console.log("===============================================================");

function kalikan (a, b) {
	return a * b
}

var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali)

console.log("===============================================================");
console.log("\n");

// SOAL 3

console.log("SOAL 3");
console.log("===============================================================");

function introduce (a, b, c, d) {
	var hasilIntroduce = "Nama saya " + a + ", umur saya " + b + " tahun, alamat saya di " + c + ", dan saya punya hobby yaitu " + d;
	return hasilIntroduce
}

var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) 

console.log("===============================================================");
console.log("\n");

// SOAL 4

console.log("SOAL 4");
console.log("===============================================================");

var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]

var objDaftarPeserta = {};
objDaftarPeserta.nama = arrayDaftarPeserta[0];
objDaftarPeserta.jenis_kelamin = arrayDaftarPeserta[1];
objDaftarPeserta.hobi = arrayDaftarPeserta[2];
objDaftarPeserta.tahun_lahir = arrayDaftarPeserta[3];

console.log(objDaftarPeserta); 

console.log("===============================================================");
console.log("\n");

// SOAL 5

console.log("SOAL 5");
console.log("===============================================================");

var buah = [{nama: "strawberry", warna: "merah", adaBijinya: true,harga: 9000}, {nama: "jeruk",warna: "oranye", adaBijinya: true, harga: 8000}, {nama: "Semangka", warna: "Hijau & Merah", adaBijinya: true, harga: 10000}, {nama: "Pisang", warna: "Kuning", adaBijinya: false, harga: 5000, harga: 10000}]

console.log(buah[0]);

console.log("===============================================================");
console.log("\n");

// SOAL 6

console.log("SOAL 6");
console.log("===============================================================");

function buatObjectDataFilm (nama, durasi, genre, tahun){
	var dataFilm ={};
	dataFilm.nama = nama;
	dataFilm.durasi = durasi;
	dataFilm.genre = genre;
	dataFilm.tahun = tahun;
	return dataFilm;
}

// contoh data film
var dataFilm1 = buatObjectDataFilm ("cinta", "20 menit", "indie", "2001")

console.log(dataFilm1);

console.log("===============================================================");
console.log("\n");


