// SOAL 1

console.log("SOAL 1");
console.log("===============================================================");

console.log("LOOPING PERTAMA");
var init = 2;
while(init <= 20) { 
  console.log(init + ' - I love coding'); 
  init = init + 2; 
}

console.log("LOOPING KEDUA");
var init2 = 20;
while(init2 >= 2) { 
  console.log(init2 + ' - I will become a frontend developer'); 
  init2 = init2 - 2;
}

console.log("===============================================================");
console.log("\n");

// SOAL 2
console.log("SOAL 2");
console.log("===============================================================");

for(var init3 = 1; init3 <= 20; init3++){
	if (init3%3 === 0 && init3%2 === 1) {
		console.log(init3 + ' - I Love Coding');	
	} else if (init3%2 === 1) {
		console.log(init3 + ' - Santai');
	} else if (init3%2 === 0) {
		console.log(init3 + ' - Berkualitas');
	}
}


console.log("===============================================================");
console.log("\n");

// SOAL 3
console.log("SOAL 3");
console.log("===============================================================");

var init4 = 7;
var hastag = '';
for(var init5 = 0; init5 < init4;  init5 ++ ) {
	for(var init6 = 0; init6 <= init5; init6 ++) {
		hastag += '#';
} 

	hastag += '\n';

}
	
console.log(hastag);

console.log("===============================================================");
console.log("\n");

// SOAL 4

console.log("SOAL 4");
console.log("===============================================================");

var kalimat="saya sangat senang belajar javascript"
var splitKalimat = kalimat.split(" ");
console.log(splitKalimat);

console.log("===============================================================");
console.log("\n");

// SOAL 5
console.log("SOAL 5");
console.log("===============================================================");

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];


for(var init7 = 0; init7 < daftarBuah.length;  init7 ++ ) {
	var urutDaftarBuah = daftarBuah.sort();	
	console.log(urutDaftarBuah[init7])
	hastag += '\n';

}

console.log("===============================================================");
