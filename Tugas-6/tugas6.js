// SOAL 1

console.log("SOAL 1");
console.log("===============================================================");

const luasLingkaran = (jari2) => 3.14 * jari2 * jari2;
let kelilingLingkaran = (jari2) => 2 * 3.14 * jari2;

console.log(luasLingkaran(5));
console.log(kelilingLingkaran(5));

console.log("===============================================================");
console.log("\n");

// SOAL 2

console.log("SOAL 2");
console.log("===============================================================");

let kalimat ="";

const buatKalimat = (parameterKalimat) => `${parameterKalimat} ${'saya'} ${'adalah'} ${'seorang'} ${'frontend'} ${'developers'}`; 

console.log(buatKalimat(kalimat));

console.log("===============================================================");
console.log("\n");


// SOAL 3
console.log("SOAL 3");
console.log("===============================================================");

const newFunction = (firstName, lastName) => {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: () => console.log(`${firstName} ${lastName}`)
  }
}

//Driver Code 
newFunction("William", "Imoh").fullName()

console.log("===============================================================");
console.log("\n");
 
// SOAL 4

console.log("SOAL 4");
console.log("===============================================================");

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject;
console.log(firstName, lastName, destination, occupation)


console.log("===============================================================");
console.log("\n");



// SOAL 5

console.log("SOAL 5");
console.log("===============================================================");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east];
//Driver Code
console.log(combined);


console.log("===============================================================");
console.log("\n");


















