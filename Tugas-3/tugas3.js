// SOAL 1

console.log("SOAL 1");
console.log("===============================================================");

// Solusi 1 dengan String Methods .concat

// var kataPertama = "saya ";
// var kataKedua = "senang ";
// var kataKetiga = "belajar ";
// var kataKeempat = "javascript";

// console.log(kataPertama.concat(kataKedua[0].toUpperCase() + kataKedua.slice(1), kataKetiga, kataKeempat.toUpperCase()));

// Solusi 2 dengan Method toUpperCase dan slice
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

console.log(kataPertama + ' ' + kataKedua[0].toUpperCase() + kataKedua.slice(1) + ' ' + kataKetiga + ' ' + kataKeempat.toUpperCase());

// Solusi 3 dengan method replace

// console.log(kataPertama + ' ' + kataKedua.replace("senang", "Senang") + ' ' + kataKetiga + ' ' + kataKeempat.toUpperCase());

console.log("===============================================================");
console.log("\n");

// SOAL 2

console.log("SOAL 2");
console.log("===============================================================");

var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

// Solusi 1 dengan menggunakan fungsi global nunmber()

// var angkaKataPertama = Number(kataPertama);
// var angkaKataKedua = Number(kataKedua);
// var angkaKataKetiga = Number(kataKetiga);
// var angkaKataKeempat = Number(kataKeempat);

// console.log(angkaKataPertama + angkaKataKedua + angkaKataKetiga + angkaKataKeempat );

// Solusi 2 dengan menggunakan fungsi global parseInt()

var angkaKataPertama = parseInt(kataPertama);
var angkaKataKedua = parseInt(kataKedua);
var angkaKataKetiga = parseInt(kataKetiga);
var angkaKataKeempat = parseInt(kataKeempat);

console.log(angkaKataPertama + angkaKataKedua + angkaKataKetiga + angkaKataKeempat );

console.log("===============================================================");
console.log("\n");

// SOAL 3
console.log("SOAL 3");
console.log("===============================================================");

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); 
var kataKetiga = kalimat.substring(15, 18); 
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

console.log("===============================================================");
console.log("\n");

// SOAL 4
console.log("SOAL 1");
console.log("===============================================================");

var nilai;
nilai = 75;

if ( nilai >= 80 ) {
    console.log("Index A")
} else if ( nilai >= 70 && nilai < 80 ) {
    console.log("Index B")
} else if ( nilai >= 60 && nilai < 70 ) {
    console.log("Index C") 
} else if ( nilai >= 50 && nilai < 60 ) {
    console.log("Index D")
}else {
    console.log("Index E")
}

console.log("===============================================================");
console.log("\n");

// soal 5
console.log("SOAL 5");
console.log("===============================================================");

var tanggal = 9;
var bulan = 7;
var tahun = 1988;

switch(bulan) {
  case 1:   { console.log(tanggal + " " + "Januari" + " " + tahun); break; }
  case 2:   { console.log(tanggal + " " + "Februari" + " " + tahun); break; }
  case 3:   { console.log(tanggal + " " + "Maret" + " " + tahun); break; }
  case 4:   { console.log(tanggal + " " + "April" + " " + tahun); break; }
  case 5:   { console.log(tanggal + " " + "Mei" + " " + tahun); break; }
  case 6:   { console.log(tanggal + " " + "Juni" + " " + tahun); break; }
  case 7:   { console.log(tanggal + " " + "Juli" + " " + tahun); break; }
  case 8:   { console.log(tanggal + " " + "Agustus" + " " + tahun); break; }
  case 9:   { console.log(tanggal + " " + "September" + " " + tahun); break; }
  case 10:   { console.log(tanggal + " " + "Oktober" + " " + tahun); break; }
  case 11:   { console.log(tanggal + " " + "November" + " " + tahun); break; }
  case 12:   { console.log(tanggal + " " + "Desember" + " " + tahun); break; }
  default:  { console.log('Masukkan angka 1 - 12 pada variabel bulan'); }}

console.log("===============================================================");

