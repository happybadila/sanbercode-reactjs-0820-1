// SOAL 1

console.log("SOAL 1");
console.log("release0   ==============================================");
class Animal {
    // Code class di sini
    constructor (name) {
	this.name = name;
    }
    get legs() {
    	return 4;
    }
    get cold_blooded() {
	return false;
    }
    
};
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


console.log("release1   ==============================================");
// Code class Ape dan class Frog di sini
class Ape extends Animal {
    constructor (name) {
	super(name);	
    }

    get legs() {
    	return 2;
    }
    
    yell(){
	console.log("Auooo");
    } 
};

class Frog extends Animal {
    constructor (name) {
	super(name);	
    }
    
    jump(){
	console.log("hop hop");
    } 
};


 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log("===============================================================");
console.log("\n");

// SOAL 2

console.log("SOAL 2");
console.log("===============================================================");

class Clock {
    // Code di sini
    constructor ({template}){
	this.template = template
	this.timer = null;

    }
    
    render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = this.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
    }

    stop(){
    	clearInterval(this.timer);
    };

    start(){
    	this.render();
    	this.timer = setInterval(this.render.bind(this), 1000);
  };	

}

var clock = new Clock({template: 'h:m:s'});
clock.start();  

console.log("===============================================================");
console.log("\n");
